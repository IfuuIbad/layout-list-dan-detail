package me.ifuu.livid.Adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<String> mTitleList = new ArrayList<>();


    public ViewPagerAdapter(FragmentManager fm) { super(fm); }

    @Override
    public Fragment getItem(int position) { return mFragmentList.get(position); }

    public void addFrag(Fragment fragment, String title)
    {
        mFragmentList.add(fragment);
        mTitleList.add(title);
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }


    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
