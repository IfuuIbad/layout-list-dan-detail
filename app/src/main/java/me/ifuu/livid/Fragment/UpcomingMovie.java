package me.ifuu.livid.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ifuu.livid.Adapter.Listener.EndlessOnScrollListener;
import me.ifuu.livid.Adapter.RecyclerViewAdapter;
import me.ifuu.livid.Data.Api;
import me.ifuu.livid.Data.Model.Movie;
import me.ifuu.livid.DetailFilm;
import me.ifuu.livid.R;


public class UpcomingMovie extends Fragment {

    private RecyclerView mRcv;
    private ProgressBar progressBar;
    private RecyclerViewAdapter mAdapter;
    // Required empty public constructor
    private Parcelable recyclerViewState;
    public RecyclerView.LayoutManager gridLayoutManager;

    ArrayList<Movie> movies = new ArrayList<>();

    public UpcomingMovie() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_movie, container, false);

        mRcv = view.findViewById(R.id.rcvUpcoming);
        mRcv.addOnScrollListener(scrollData());
        progressBar = view.findViewById(R.id.progressBar);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        // getting data from api and display that
        String url = Api.MOVIE_UPCOMMING("1");
        // getting data from api and display that
        fan(url);
        return view;
    }

    private EndlessOnScrollListener scrollData()
    {
        return new EndlessOnScrollListener() {
            @Override
            public void onLoadmore() {
                int totalItem = movies.size();
                int nextPage = (totalItem / 20) + 1;

                String url = Api.MOVIE_UPCOMMING(String.valueOf(nextPage));
                fan(url);


            }
        };
    }

    private void fan(String url)
    {
        //display a progressbar
        progressBar.setVisibility(View.VISIBLE);


        //refresh or reset recycler view and array list

        AndroidNetworking.get(url)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("_total_page", String.valueOf(response.optInt("total_pages")));
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = response.getJSONArray("results");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject jsonObject = null;
                            try {

                                jsonObject = jsonArray.getJSONObject(i);
                                // adding data to ArrayList
                                Movie movie = new Movie();
                                movie.setId(jsonObject.optInt("id"));
                                movie.setTitle(jsonObject.optString("title"));
                                movie.setImg(jsonObject.optString("poster_path"));
                                movies.add(movie);




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                        // Display a recyclerView
                        mAdapter = new RecyclerViewAdapter(movies);
                        mRcv.setLayoutManager(gridLayoutManager);
                        recyclerViewState = mRcv.getLayoutManager().onSaveInstanceState();
                        mRcv.setAdapter(mAdapter);


                        mRcv.getLayoutManager().onRestoreInstanceState(recyclerViewState);


                        mAdapter.setOnItemClickListener(new RecyclerViewAdapter.ListItemListener() {
                            @Override
                            public void OnItemClicked(int position) {
                                actionClick(position);
                            }
                        });

                        // hidden a progressbar
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void actionClick(int position)
    {
        Intent intent = new Intent(getActivity(), DetailFilm.class);
        int id = movies.get(position).getId();
        String title = movies.get(position).getTitle();
        intent.putExtra("_id", id);
        intent.putExtra("_title", title);
        String poster = movies.get(position).getImg();
        intent.putExtra("_poster", poster);
        startActivity(intent);

//        Log.i("_position", String.valueOf(position));
    }


}
