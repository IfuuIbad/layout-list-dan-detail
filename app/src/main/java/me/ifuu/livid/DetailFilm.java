package me.ifuu.livid;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Movie;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.ifuu.livid.Adapter.ListAdapter;
import me.ifuu.livid.Data.Api;
import me.ifuu.livid.Data.Model.FavMovie;
import me.ifuu.livid.Data.Model.TrailerMovie;
import me.ifuu.livid.Utils.DownloadImage;

public class DetailFilm extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.Poster) ImageView mPoster;
    @BindView(R.id.smallPoster) ImageView mSPoster;
    @BindView(R.id.date) TextView mDate;
    @BindView(R.id.rate) TextView mRate;
    @BindView(R.id.desc) TextView mDesc;
    @BindView(R.id.duration) TextView mDuration;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.progressBar) ProgressBar mProgressbar;
    @BindView(R.id.rcvTrailer) RecyclerView mRcv;
    @BindView(R.id.btnFav) FloatingActionButton btnFav;
    @BindView(R.id.btnDell) FloatingActionButton btnDell;
    private ListAdapter mAdapter;
    public RecyclerView.LayoutManager linearLayoutManager;
    private ArrayList<TrailerMovie> trailers = new ArrayList<>();
    private FavMovie movie;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_film);
        ButterKnife.bind(this);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
//        movie = MyApp.db.lividDao().getAll();

        // pick data intent.extras
        Bundle bundle = getIntent().getExtras();
        String id = String.valueOf(bundle.getInt("_id"));
        String title = bundle.getString("_title");
        String urlDetail = Api.Detail(id);
        String urlTrailer = Api.TRAILER(id);
        if (currentUser != null) {
            movie = MyApp.db.lividDao().selectOneRow(id, currentUser.getUid());
        }
        if (movie == null) {
            btnFav.setVisibility(View.VISIBLE);
            btnDell.setVisibility(View.INVISIBLE);

        } else {
            btnFav.setVisibility(View.INVISIBLE);
            btnDell.setVisibility(View.VISIBLE);
        }


        mTitle.setText(title);

        // display data
        fanDetail(urlDetail);
        fanTrailer(urlTrailer);

        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFav();
            }
        });
        btnDell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delFav();
            }
        });

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }


    public void addFav()
    {
        if (currentUser != null) {
            Bundle bundle = getIntent().getExtras();
            final String id = String.valueOf(bundle.getInt("_id"));
            final String title = bundle.getString("_title");
            final String poster = bundle.getString("_poster");

            FavMovie addMovie = new FavMovie();
            addMovie.setUId(currentUser.getUid());
            addMovie.setTmdbId(id);
            addMovie.setTitle(title);
            addMovie.setImg(poster);

            MyApp.db.lividDao().insert(addMovie);

            Toast.makeText(this, "Film telah di ditambahkan ke daftar favorite", Toast.LENGTH_SHORT).show();
            btnFav.setVisibility(View.INVISIBLE);
            btnDell.setVisibility(View.VISIBLE);
        }else{
            showDialogMessage();
        }
    }

    public void delFav()
    {
        Bundle bundle = getIntent().getExtras();
        String id = String.valueOf(bundle.getInt("_id"));
        movie = MyApp.db.lividDao().selectOneRow(id, currentUser.getUid());
        MyApp.db.lividDao().delete(movie);
        Toast.makeText(this, "Film telah terhapus dari daftar favorite", Toast.LENGTH_SHORT).show();
        btnFav.setVisibility(View.VISIBLE);
        btnDell.setVisibility(View.INVISIBLE);
    }

    public void showDialogMessage()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailFilm.this);
        builder.setTitle("error");
        builder.setMessage("to add favorite you must Login first, do you want Login now ? .");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "cancelled", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void fanDetail(String url)
    {
        AndroidNetworking.get(url)
                .addPathParameter("pageNumber", "0")
                .addQueryParameter("limit", "3")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject cover = null;
                        try {
                            cover = response.getJSONObject("belongs_to_collection");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                           if (cover != null)
                           {
                               DownloadImage.picasso(Api.POSTER_PATH + cover.optString("poster_path"), mPoster);
                           }else
                           {
                               DownloadImage.picasso(Api.POSTER_PATH + response.optString("poster_path"), mPoster);
                           }

                        DownloadImage.picasso(Api.POSTER_PATH + response.optString("poster_path"), mSPoster);
                        try {
                            mDate.setText(response.getString("release_date"));
                            mRate.setText(String.valueOf(response.optDouble("vote_average")));
                            mDesc.setText(response.optString("overview"));
                            mDuration.setText(response.optString("runtime")+" min");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });

    }

    public void fanTrailer(String url)
    {
        mProgressbar.setVisibility(View.VISIBLE);

        AndroidNetworking.get(url)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray results = null;
                        try {
                            results = response.getJSONArray("results");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        for (int i = 0; i < results.length(); i++)
                        {
                            JSONObject result = null;

                            try {
                                result = results.getJSONObject(i);
                                TrailerMovie trailer = new TrailerMovie();
                                trailer.setmImageResource(R.drawable.ic_play_circle);
                                trailer.setmTitle(result.optString("name"));
                                trailer.setId(result.optInt("id"));
                                trailer.setmSite(result.optString("site"));
                                trailer.setmKey(result.optString("key"));
                                trailers.add(trailer);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                     linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                    mAdapter = new ListAdapter(trailers);
                    mRcv.setLayoutManager(linearLayoutManager);
                    mRcv.setAdapter(mAdapter);

                    mAdapter.setOnItemClickListener(new ListAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            String key = trailers.get(position).getmKey();
                            String url = Api.URL_YOUTUBE(key);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    });

                    mProgressbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.toolbarChat:
                showCommentFilm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showCommentFilm()
    {
        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
        Bundle bundle = getIntent().getExtras();
        String id = String.valueOf(bundle.getInt("_id"));
        intent.putExtra("_id", id);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish(); // untuk menutup actifity
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
    }
}
