package me.ifuu.livid;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.ifuu.livid.Adapter.ViewPagerAdapter;
import me.ifuu.livid.Fragment.FavoriteMovie;
import me.ifuu.livid.Fragment.PopularMovie;
import me.ifuu.livid.Fragment.UpcomingMovie;

public class MainTabs extends AppCompatActivity {

    @BindView(R.id.vPager) ViewPager vPager;
    @BindView(R.id.tabs) TabLayout tabs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tabs);
        ButterKnife.bind(this);

        addTabs(vPager);
        tabs.setupWithViewPager(vPager);

    }

    private void addTabs(ViewPager vPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new PopularMovie(), "Popular");
        adapter.addFrag(new UpcomingMovie(), "Upcomming");
        adapter.addFrag(new FavoriteMovie(), "Favorite");
        vPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbarProfile:
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    showProfile();
                }else{
                    showLoginFrom();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showLoginFrom(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    private void showProfile(){
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(intent);
    }


}
