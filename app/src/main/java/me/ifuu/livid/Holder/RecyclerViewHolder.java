package me.ifuu.livid.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import me.ifuu.livid.Adapter.RecyclerViewAdapter;
import me.ifuu.livid.R;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView mTitle;
    public ImageView mPoster;



    public RecyclerViewHolder(View itemView, final RecyclerViewAdapter.ListItemListener listener) {
        super(itemView);

        mTitle = itemView.findViewById(R.id.txtTitle);
        mPoster = itemView.findViewById(R.id.filmPoster);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                    {
                        listener.OnItemClicked(position);
                    }
                }
            }
        });

    }
}
