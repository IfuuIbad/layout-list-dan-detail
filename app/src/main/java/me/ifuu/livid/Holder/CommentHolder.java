package me.ifuu.livid.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.library.bubbleview.BubbleTextView;

import de.hdodenhof.circleimageview.CircleImageView;
import me.ifuu.livid.Adapter.CommentAdapter.ListItemListener;
import me.ifuu.livid.R;

public class CommentHolder extends RecyclerView.ViewHolder{
    public CircleImageView mImage;
    public TextView mName, mDate;
    public BubbleTextView mComment;

    public CommentHolder(View itemView, final ListItemListener listener) {
        super(itemView);

        mImage = itemView.findViewById(R.id.users_img);
        mName = itemView.findViewById(R.id.user_name);
        mComment = itemView.findViewById(R.id.user_comment);
        mDate = itemView.findViewById(R.id.onCreate);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                    {
                        listener.OnItemCliked(position);
                    }
                }
            }
        });
    }

}
