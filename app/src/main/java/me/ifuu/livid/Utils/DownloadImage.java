package me.ifuu.livid.Utils;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import me.ifuu.livid.R;

public class DownloadImage {
    public static void picasso(String url, ImageView imageView)
    {
        Picasso.get().load(url)
                     .placeholder(R.drawable.ic_image)
                     .error(R.drawable.ic_error)
                     .fit().centerCrop().into(imageView);
    }

    public static void picassoProfile(String url, ImageView imageView)
    {
        Picasso.get().load(url)
                .placeholder(R.drawable.ic_image)
                .resize(100,100)
                .error(R.drawable.ic_image)
                .centerCrop().into(imageView);
    }

    public static void picassoComment(String url, ImageView imageView)
    {
        Picasso.get().load(url)
                .placeholder(R.drawable.ic_image)
                .resize(50,50)
                .error(R.drawable.ic_image)
                .centerCrop().into(imageView);
    }
}
